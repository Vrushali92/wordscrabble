//
//  ViewController.swift
//  Project5
//
//  Created by Vrushali Kulkarni on 14/05/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import UIKit

final class ViewController: UITableViewController {

    var allWords = [String]()
    var usedWords = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        loadWordsFromFile()
        startGame()
    }
}

private extension ViewController {

    func configureUI() {

        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add,
                                                            target: self,
                                                            action: #selector(promptForAnswer))

        navigationItem.leftBarButtonItem  = UIBarButtonItem(title: "New",
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(startGame))
    }

    func loadWordsFromFile() {

        if let startWordsURL = Bundle.main.url(forResource: "start", withExtension: "txt"),
            let startWords = try? String(contentsOf: startWordsURL) {
            allWords = startWords.components(separatedBy: "\n")
        }

        if allWords.isEmpty {
            allWords = ["silkworm"]
        }
    }

    @objc func startGame() {

        title = allWords.randomElement()
        usedWords.removeAll(keepingCapacity: true)
        tableView.reloadData()
    }

    @objc func promptForAnswer() {

        let alertController = UIAlertController(title: "Enter answer",
                                                message: "",
                                                preferredStyle: .alert)
        alertController.addTextField()
        let submitAction = UIAlertAction(title: "Submit",
                                         style: .default) { [weak self, weak alertController] _ in
                                            guard let answer = alertController?.textFields?[0].text else { return }
                                            self?.submit(answer)
        }
        alertController.addAction(submitAction)
        present(alertController, animated: true, completion: nil)
    }

    func submit(_ answer: String) {

        guard !answer.isEmpty, answer.count > 3 else { return }
        let lowerAnswer = answer.lowercased()
        let errorTitle: String
        let errorMessage: String

        if isPossible(word: lowerAnswer) {
            if isOriginal(word: lowerAnswer) {
                if isReal(word: lowerAnswer) {

                    usedWords.insert(lowerAnswer, at: 0)
                    let indexPath = IndexPath(row: 0, section: 0)
                    tableView.insertRows(at: [indexPath], with: .automatic)
                    return
                } else {
                    errorTitle = "Word not recognised"
                    errorMessage = "You can't just make them up, you know!"
                }
            } else {
                errorTitle = "Word used already"
                errorMessage = "Be more original!"
            }
        } else {
            guard let title = title?.lowercased() else { return }
            errorTitle = "Word not possible"
            errorMessage = "You can't spell that word from \(title)"
        }

        showAlert(with: errorTitle, and: errorMessage)
    }

    func showAlert(with title: String, and message: String) {

        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Continue",
                                        style: .default,
                                        handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }

    func isPossible(word: String) -> Bool {
        guard var tempWord = title?.lowercased() else { return false }

        for letter in word {
            if let position = tempWord.firstIndex(of: letter) {
                tempWord.remove(at: position)
            } else {
                return false
            }
        }
        return true
    }

    func isReal(word: String) -> Bool {
        let checker = UITextChecker()
        let range = NSRange(location: 0, length: word.utf16.count)
        let misspelledRange = checker.rangeOfMisspelledWord(in: word,
                                                            range: range,
                                                            startingAt: 0,
                                                            wrap: false,
                                                            language: "en")
        return misspelledRange.location == NSNotFound
    }

    func isOriginal(word: String) -> Bool {
        return !usedWords.contains(word)
    }
}

extension ViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usedWords.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Word", for: indexPath)
        cell.textLabel?.text = usedWords[indexPath.row]
        return cell
    }
}

